/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.oxgui;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wittaya
 */
public class friend implements Serializable{
    private int id;
    private String name;
    private int age;
    private String tel;
    private static int lastID=1;
    public friend(String name,int age,String tel){
        this.id = lastID++;
        this.name = name;
        this.age = age;
        this.tel = tel;
        
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws Exception {
        if(age<0){
            throw new Exception();
        }
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "friend{" + "id=" + id + ", name=" + name + ", age=" + age + ", tel=" + tel + '}';
    }
    
    public static void main(String[] args) {
        friend f = new friend("pom",70,"1234");
        try {
            f.setAge(-1);
        } catch (Exception ex) {
            System.out.println("age is lower than 0");
        }
        System.out.println(""+f);
    }
}
    